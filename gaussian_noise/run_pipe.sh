#!/bin/bash

inifile=config.ini
workdir=gaussianNoise

# (For simulated data like this the trigtime is doesn't actually matter)
trigtime=1126259462.42 

bayeswave_pipe \
    --workdir ${workdir} \
    --sim-data \
    -t ${trigtime} \
    ${inifile} 

# --sim-data required for simulated data