===================
BayesWave Workflow
===================

:code:`BayesWave` and the postprocessing script :code:`BayesWavePost` are written in C.
There are also python utilities used to set up the :code:`BayesWave` run (:code:`bayeswave_pipe`),
and make plots and output webpages. (:code:`megaplot.py`).  The order of executables is:

:code:`bayeswave_pipe`
-------------------------
This reads in a config file that contains information such as what data to look at, whether these are injections, and general run setting. This then sets up the BayesWave job(s) to be submitted to condor. This can produce a dag file for either a single event, or a batch of events. :code:`bayeswave_pipe` uses the python module :code:`ConfigArgParse`.

:code:`BayesWave`
-------------------------
This is the main part of the code. This reads in detector data (real or simulated) and performs the RJMCMC. The main outputs of :code:`BayesWave` are chain files.

:code:`BayesWavePost`
-------------------------
Processes the raw chains output by :code:`BayesaWave` to make more human-readable outputs, such as posterior distributions on waveforms.  :code:`BayesWavePost` is not always required -- for example for large batches of runs where only the Bayes factor is of interest it is not recommended to run :code:`BayesWavePost` for every trigger to save both computing time and disk space. To skip this part, include :code:`--skip-post` in the :code:`bayeswave_pipe` call.

:code:`megaplot.py`
-------------------------
Can only be run after :code:`BayesWavePost` has been run. Uses the output of :code:`BayesWavePost` to make plots and output webpages. Again, :code:`megaplot` is not required for every run. If you are not planning on looking at the output webpages, there is no need to run :code:`megaplot`. To skip this part, include :code:`--skip-megapy` in the :code:`bayeswave_pipe` call.

:code:`megasky.py`
-------------------------
NOW DEFUNCT. For power users only: will make healpix maps of the sky position posteriors.

