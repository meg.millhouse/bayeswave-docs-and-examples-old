================================================
Use of BayesWave in Scientific Publications
================================================

License
---------

BayesWave uses the `GNU General Public License v2 <https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html>`_. 

Citing BayesWave
------------------

Citing the scientific publications that describe the algorithms
==================================================================
We request that publications and other work arising from use of this code cite the following:

.. code-block:: tex

    @ARTICLE{2015CQGra..32m5012C,
        author = {{Cornish}, Neil J. and {Littenberg}, Tyson B.},
        title = "{Bayeswave: Bayesian inference for gravitational wave bursts and
        instrument glitches}",
        journal = {Classical and Quantum Gravity},
        keywords = {General Relativity and Quantum Cosmology, Astrophysics - High Energy
        Astrophysical Phenomena, Astrophysics - Instrumentation and
        Methods for Astrophysics},
        year = 2015,
        month = Jul,
        volume = {32},
        eid = {135012},
        pages = {135012},
        doi = {10.1088/0264-9381/32/13/135012},
        archivePrefix = {arXiv},
        eprint = {1410.3835},
        primaryClass = {gr-qc},
        adsurl = {https://ui.adsabs.harvard.edu/#abs/2015CQGra..32m5012C},
        adsnote = {Provided by the SAO/NASA Astrophysics Data System}
        }

    @ARTICLE{2015PhRvD..91h4034L,
        author = {{Littenberg}, Tyson B. and {Cornish}, Neil J.},
        title = "{Bayesian inference for spectral estimation of gravitational wave detector noise}",
        journal = {Physical Review D},
        keywords = {04.30.-w, 04.80.Nn, 95.55.Ym, Gravitational waves: theory, Gravitational wave detectors and experiments, Gravitational radiation detectors, mass spectrometers, and other instrumentation and techniques, General Relativity and Quantum Cosmology, Astrophysics - High Energy Astrophysical Phenomena, Astrophysics - Instrumentation and Methods for Astrophysics},
        year = 2015,
        month = apr,
        volume = {91},
        number = {8},
        eid = {084034},
        pages = {084034},
        doi = {10.1103/PhysRevD.91.084034},
        archivePrefix = {arXiv},
        eprint = {1410.3852},
        primaryClass = {gr-qc},
        adsurl = {https://ui.adsabs.harvard.edu/abs/2015PhRvD..91h4034L},
        adsnote = {Provided by the SAO/NASA Astrophysics Data System}
        }

    @ARTICLE{2021PhRvD.103d4006C,
        author = {{Cornish}, Neil J. and {Littenberg}, Tyson B. and {B{\'e}csy}, Bence and {Chatziioannou}, Katerina and {Clark}, James A. and {Ghonge}, Sudarshan and {Millhouse}, Margaret},
        title = "{BayesWave analysis pipeline in the era of gravitational wave observations}",
        journal = {Physical Review D},
        keywords = {General Relativity and Quantum Cosmology, Astrophysics - High Energy Astrophysical Phenomena},
        year = 2021,
        month = feb,
        volume = {103},
        number = {4},
        eid = {044006},
        pages = {044006},
        doi = {10.1103/PhysRevD.103.044006},
        archivePrefix = {arXiv},
        eprint = {2011.09494},
        primaryClass = {gr-qc},
        adsurl = {https://ui.adsabs.harvard.edu/abs/2021PhRvD.103d4006C},
        adsnote = {Provided by the SAO/NASA Astrophysics Data System}
        }


Citing the BayesWave Software
==================================================================

We also request that software developed from this code refers the URL of this project's repository: https://git.ligo.org/lscsoft/bayeswave,
and indicates the code version (via e.g., tag and/or git hash) used for development.



