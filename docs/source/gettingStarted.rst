============
Installation
============


Option 1: Running on CIT
-------------------------
If you are running on CIT, you can use environments with already existing `BayesWave` installations instead of installing yourself. The recommended environment is currently `igwn-py39-testing`.


Option 2: Conda forge
-------------------------
.. code-block:: console

    $ conda install -c conda-forge bayeswave

Option 3: Installing from source
---------------------------------
*Note: it is recommended to follow the fork/merge development model*

Clone from the `bayeswave` repository

.. code-block:: console

    $ git clone git@git.ligo.org:lscsoft/bayeswave/git

Set up the conda environment. From the main :code:`bayeswave` directory, run the command

.. code-block:: console

    $ conda env create -f environment.yaml

Activate the environment via

.. code-block:: console

    $ conda activate bayeswave

Still from the :code:`bayeswave` directory, install via

.. code-block:: console

    $ export BAYESWAVE_PREFIX=${HOME}/opt/lscsoft/bayeswave
    $ ./install.sh ${BAYESWAVE_PREFIX}

.. note::

   Instead of :code:`${HOME}/opt/lscsoft/bayeswave` you can also use a directory of your choosing

Upon successful installation, you should be prompted to source the environment via

.. code-block:: console

    $ source ${BAYESWAVE_PREFIX}/etc/bayeswave-user-env.sh

*TODO: is it confusing that there are two environments??*
