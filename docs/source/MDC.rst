================================================
Burst style MDCs
================================================

Instructions for running burst-style MDCs.
This requires MDC frames in the :code:`.gwf` format.

Making an MDC cache
---------------------

:code:`BayesWave` requires a path to the cache file of the MDC frames.
You can make a cache file vanilla

.. code:: console

    $ lal_path2cache directory_with_frames/*gwf > MDC.cache

Injecting into real noise
---------------------------

Config file
~~~~~~~~~~~~~~~

.. code:: ini

    [input]
    dataseed=1234
    seglen=4.0
    window=1.0
    flow=32
    srate=2048

    ifo-list=['H1','L1']

    [engine]
    install=/home/meg.millhouse/Development_BW/stable
    bayeswave=%(install)s/bin/BayesWave
    bayeswave_post=%(install)s/bin/BayesWavePost
    megaplot=%(install)s/bin/megaplot.py


    [datafind]
    channel-list={'H1':'H1:DCH-CLEAN_STRAIN_C02','L1':'L1:DCH-CLEAN_STRAIN_C02'}
    frtype-list={'H1':'H1_CLEANED_HOFT_C02','L1':'L1_CLEANED_HOFT_C02'}
    url-type=file
    veto-categories=[1]

    [injections]
    mdc-cache=/home/meg.millhouse/Development_BW/bayeswave-docs-and-examples/MDC/MDC_caches/MDC.cache
    mdc-channels={'H1':'H1:SCIENCE','L1':'L1:SCIENCE'}
    mdc-prefactor=1

    [bayeswave_options]
    ; command line options for BayesWave.  See BayesWave --help
    bayesLine=
    updateGeocenterPSD=
    waveletPrior=
    Dmax=100
    Niter=5000

    [bayeswave_post_options]
    ; command line options for BayesWavePost.  See BayesWavePost --help
    bayesLine=
    0noise=

    [condor]
    ; see e.g., https://ldas-gridmon.ligo.caltech.edu/ldg_accounting/user
    ;accounting-group = ligo.prod.o1.burst.paramest.bayeswave
    universe=vanilla
    checkpoint=
    bayeswave-request-memory=1000
    bayeswave_post-request-memory=4000
    bayeswave-request-disk=100
    bayeswave_post-request-disk=100
    datafind=/usr/bin/gw_data_find
    ligolw_print=/usr/bin/ligolw_print
    segfind=/usr/bin/ligolw_segment_query_dqsegdb
    accounting-group = ligo.dev.o4.burst.paramest.bayeswave


Call to :code:`bayeswave_pipe`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash

    #!/bin/bash

    # If using system install on CIT, you can delete these tow lines 
    BW_PREFIX=/home/meg.millhouse/Development_BW/stable
    source ${BW_PREFIX}/etc/bayeswave-user-env.sh

    bayeswave_pipe \
        --workdir WNB_MDC \
        -t 1174112418.84 \
        MDC.ini

Injecting into simulated data
--------------------------------------

Config file
~~~~~~~~~~~~~~~

.. code:: ini

    [input]
    dataseed=1234
    seglen=4.0
    window=1.0
    flow=32
    srate=2048

    ifo-list=['H1','L1']

    [engine]
    install=/home/meg.millhouse/Development_BW/stable
    bayeswave=%(install)s/bin/BayesWave
    bayeswave_post=%(install)s/bin/BayesWavePost
    megaplot=%(install)s/bin/megaplot.py


    [datafind]
    channel-list={'H1':'H1:LALSimAdLIGO','L1':'L1:LALSimAdLIGO'}
    frtype-list={'H1':'LALSimAdLIGO','L1':'LALSimAdLIGO'}
    url-type=file
    veto-categories=[1]

    [injections]
    mdc-cache=/home/meg.millhouse/Development_BW/bayeswave-docs-and-examples/MDC/MDC_caches/MDC.cache
    mdc-channels={'H1':'H1:SCIENCE','L1':'L1:SCIENCE'}
    mdc-prefactor=1

    [bayeswave_options]
    ; command line options for BayesWave.  See BayesWave --help
    updateGeocenterPSD=
    waveletPrior=
    Dmax=100
    Niter=5000

    [bayeswave_post_options]
    ; command line options for BayesWavePost.  See BayesWavePost --help
    bayesLine=
    0noise=

    [condor]
    ; see e.g., https://ldas-gridmon.ligo.caltech.edu/ldg_accounting/user
    ;accounting-group = ligo.prod.o1.burst.paramest.bayeswave
    universe=vanilla
    checkpoint=
    bayeswave-request-memory=1000
    bayeswave_post-request-memory=4000
    bayeswave-request-disk=100
    bayeswave_post-request-disk=100
    datafind=/usr/bin/gw_data_find
    ligolw_print=/usr/bin/ligolw_print
    segfind=/usr/bin/ligolw_segment_query_dqsegdb
    accounting-group = ligo.dev.o4.burst.paramest.bayeswave

    [segfind]
    ; See e.g., https://wiki.ligo.org/viewauth/DetChar/DataQuality/AligoFlags
    segment-url=https://segments.ligo.org


Call to :code:`bayeswave_pipe`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: ini

    #!/bin/bash

    # If using system install on CIT, you can delete these two lines 
    BW_PREFIX=/home/meg.millhouse/Development_BW/stable
    source ${BW_PREFIX}/etc/bayeswave-user-env.sh

    bayeswave_pipe \
        --workdir WNB_MDC_simdata \
        -t 1174112418.84 --sim-data \
        MDC_simdata.ini