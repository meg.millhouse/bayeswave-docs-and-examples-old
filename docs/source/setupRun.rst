========================
Setting up a Run
========================

The suggest method of setting up a :code:`BayesWave` run is to make a config file,
and a bash script which calls :code:`bayeswave_pipe`.

Config files
=============

A config file will be a :code:`.ini` file, and follows common .ini  syntax.  Sections are labeled in :code:`[square brackets]`, 

:code:`[input]`
----------------
General settings for the run

Required:

- :code:`seglen=float`: duration of data (s)

- :code:`window=float`: duration of time window for model characterization runs (i.e the window where wavelets will can be placed)

- :code:`flow=float`: minimum frequency (Hz)

- :code:`srate=float`: sampling rate (Hz). Must be a power of 2.

- :code:`PSDlength=float`: GPS start time for PSD estimation

- :code:`ifolist=float`: List of IFOs to include in the analysis, for example `['H1','L1']`


Optional:

- :code:`dataseed=integer`: Required if using simulated noise. Seed for noise realisation. Integer.


:code:`[engine]`
----------------
 Information about where BayesWave executable and related scripts are

Required:

- :code:`bayeswave=str`: Path to the `BayesWave` executabe, for example `{path_to_executables}/BayesWave`
- :code:`bayeswave_post=str`: Path to `BayesWavePost` executable, e.g. `{path_to_executables}/BayesWavePost`
- :code:`megaplot=str`: Path to megaplot executable, e.g. `{path_to_executables}/megaplot.py`


:code:`[datafind]` 
-------------------
Information about the type of data to be used.  
If real using real data, sets up :code:`gw_data_find` call to make cache files.

Required:

- :code:`frtype-list=dict`: List of frame times for each IFO. Takes dictionary in form :code:`{'IFO_1':'frame_type','IFO_2':'frame_type'}`

- :code:`channel-list=dict`: List of channels for each IFO.

- :code:`url-type=file` -- information about how cache is made. 

Optional:

:code:`veto-categories=list`: Data quality vetoes



:code:`[bayeswave_options]` 
--------------------------------
Command line options for :code:`BayesWave`.  See BayesWave --help for a detailed list. 
Any flag can by passed to the `BayesWave` command line by setting :code:`exampleFlag=`, and a flag with an argument can passed with :code:`exampleFlag=arugment`.

Optional command line options for :code:`BayesWave`.:

Run parameters 
~~~~~~~~~~~~~~~~
  --segment-start SEGSTART        GPS start time of segment (trigtime + 2 - seglen) 
  --CBC-trigtime TRIGTIME         window for CBC is set around this time, any other window unchanged 
  --glitchBuster         Preform deterministic glitch subtraction as initial point in glitch model
  --Niter NITER               number of iterations (4000000)
  --Nchain NCHAIN              number of parallel chains (20)
  --Ncycle NCYCLE             number of model updates per RJMCMC iteration (100)
  --Nburnin NBURNIN             number of burn-in iterations (50000)
  --maxLogL MAXLOGL             use maximized likelihood during burnin
  --chainseed SEED           random number seed for Markov chain (1234)
  --runName NAME             run name for output files
  --outputDir DIRECTORY           absolute path of where the output should be written
  --waveletStart FILEPATH        path to files containing wavelet starting parameters
  --0noise               no noise realization
  --prior                sample from prior using logL = constant test
  --gnuplot              output files for gnuplot animations
  --verbose              output hot chains
  --window WINDOW              duration of time window for model characterization runs
  --CBCwindow WINDOW           window for CBC is set around trigtime (or CBC-trigtime if given) with this length 
  --Niter-search NITER        number of iterations used in CBC search phase (5000) 
  --checkpoint           enable self-checkpointing
  --version              print BayesWave version and exit
  --help                 print BayesWave options and exit


Model parameters
~~~~~~~~~~~~~~~~
  --chirplets            use chirplets as reconstruction basis
  --fullOnly             require signal && glitch model
  --noClean              skip cleaning phase and go right to reduced window
  --noSignal             skip signal phase and quit after glitch model
  --noGlitch             skip glitch phase
  --cleanOnly            run bayesline & glitch cleaning phase only
  --noiseOnly            use noise model only (no signal or glitches or cbc)
  --signalOnly           use signal model only (no glitches or cbc)
  --glitchOnly           use glitch model only (no signal or cbc)
  --cbcOnly              use full CBC model only (incl. extrinsic parameters, no signal or glitches)
  --CBCGlitch            use full CBC model and glitch model (incl. extrinsic parameters, no signal)
  --noPSDfit             keep PSD parameters fixed
  --bayesLine            use BayesLine for PSD model
  --bayesCBC-tidal       specify BayesCBC waveform used: NRTidal_V or NoNRT_V (default)) (
  --bayesCBC-skip-ext    skip BayesCBC extrinsic burnin (


Priors and Proposals
~~~~~~~~~~~~~~~~~~~~~~
  --Dmin MIN                   minimum number of wavelets total (1)
  --Dmax MAX                   maximum number of wavelets per channel (100)
  --fixD D                  fix # of wavelets for signal/IFO.  Overrides --Dmin,--Dmax
  --Qmin MIN                  minimum quality factor for wavelets (0.1)
  --Qmax MAX                  maximum quality factor for wavelets (40)
  --waveletFmin MIN            minimum frequency for wavelets 
  --waveletFmax MAX           maximum frequency for wavelets 
  --noWaveletPrior         revert to uniform p(D)
  --updateGeocenterPSD     geocenter PSD depends on extrinsic parameters
  --waveletPrior           use empirical distribution on number of wavelets (O1)
  --backgroundPrior FILE        name of 2-column bkg frequency distribution file
  --noOrientationProposal  disable MCMC proposal for psi/ecc
  --uniformAmplitudePrior  don't use SNR-dependent amplitude prior
  --noSignalAmplitudePrior   use same SNR-dependent prior for signal & glitch model
  --noAmplitudeProposal    don't draw from SNR-dependent amplitude prior
  --varyExtrinsicAmplitude   update wavelet amplitudes in extrinsic update    (FIXED?)
  --noPolarization         do not assume elliptical polarization for signal model
  --noClusterProposal      disable clustering & TF density proposal                 
  --clusterWeight WEIGHT         fractional weight for TF to proximity proposal (0.5)
  --ampPriorPeak PEAK          SNR where amplitude prior peaks (5)
  --signalPriorPeak PEAK        SNR where signal amplitude prior peaks (5)
  --dimensionDecayRate RATE    e-folding time for number of wavelets (1000)
  --fixIntrinsicParams     hold intrinsic parameters fixed
  --fixExtrinsicParams     hold extrinsic parameters fixed
  --loudGlitchPrior        set floor of PSD prior to .01*Sn(f=200Hz)
  --fixSky                 hold sky locaiton fixed
  --fixRA RA                 right ascension in radians of fixed sky location
  --fixDEC  DEC               declination in radians of fixed sky location
  --TFQ-resize             increases nQ/nf spacing for high srate/Qmax in TFQ proposal
  --TFQ-nf-spacing         set custom nf spacing value for TFQ proposal
  --TFQ-nQ-spacing         set custom nQ spacing value for TFQ proposal

Parallel Tempering parameters
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  --tempMin MIN             minimum temperature chain (1)
  --noAdaptTemperature   disable adjust PT ladder to maintain acc. rate across chains
  --tempSpacing SPACING          set temperature spacing for geometric ladder (1.2)
  --noSplineEvidence     disable spline thermodynamic integration

LALInference injection options
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  --inj INJFILE      Injection XML file to use
  --event N              Event number from Injection XML file to use

Burst MDC injection options
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  --MDC-channel CHANNEL          IFO1-chan, IFO2-chan, etc
  --MDC-cache CACHE           IFO1-mdcframe, IFO2-mdcframe, etc
  --MDC-prefactor PREFACTOR        Rescale injection amplitude (1.0)

BayesWave internal injection bayeswave_options
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  --BW-inject MODEL           (signal/glitch)
  --BW-injName NAME          runName that produced the chain file 
  --BW-path PATH             Path to BW chain file for injection (./chains) 
  --BW-event EVENT            Which sample from BayesWave chain (200000)


:code:`[bayeswave_post_options]`
--------------------------------
Required:

:code:`0noise=`: Always required for technical purposes.

:code:`[injections]`
--------------------------------
Optional. Use if doing injections from an xml file.

If using, required:

- :code:`events=list of events`: Flexible syntax. Can set to `all`, or for a smaller set of events a list like `0:10` or `1,3,5`.

- :code:`inj-fref=float`: Reference frequency for injection.

 .. todo:: 
    include MDC section*


:code:`[condor]`
-------------------------
Information for setting up the condor job.
More details on running condor jobs can be found at `HTCondor <https://htcondor.org/htcondor/documentation/>`_.

Required:
- :code:`universe=vanilla`

- :code:`bayeswave-request-memory=int`: Required memory for `BayesWave`

- :code:`bayeswave_post-request-memory=int`: Required memory for `BayesWavePost`

- :code:`bayeswave-request-disk=int`: Required disk space for `BayesWave`

- :code:`bayeswave_post-request-disk=int`: Required disk space for `BayesWavePost`

- :code:`datafind=/usr/bin/gw_data_find`: Specify the `gw_data_find` executable 

- :code:`ligolw_print=/usr/bin/ligolw_print`: Specify the `ligolw_print` executable 

- :code:`accounting-group=str` LIGO accounting group

Optional:
- :code:`checkpoint=`: Self checkpointing. If running on condor this should be used.


Running :code:`bayeswave_pipe`
================================

Once you have created a config file, you can run :code:`bayeswave_pipe` to set up your BayesWave run,
or your batch of runs.

positional arguments
-----------------------

  configfile

options
-----------------------

  --help, -h                                        show this help message and exit
  --workdir WORKDIR, -r WORKDIR                                        name of directory for this batch of jobs
  --trigger-time TRIGGER_TIME, -t TRIGGER_TIME      GPS trigger time
  --trigger-list TRIGGER_LIST, -l TRIGGER_LIST      List of GPS trigger times
  --cwb-trigger-list CWB_TRIGGER_LIST     cWB style trigger list
  --CBC-trigger-list CBC_TRIGGER_LIST   Trigger list for CBCs
  --bayesline-median-psd    Print out median bayesLine PSD
  --server SERVER   LIGO data find server
  --copy-frames     Copy .gwf frames (for OSG use (TODO someone check this))
  --skip-datafind       Skip :code:`gw_data_find`
  --sim-data        Use simulated data
  -I INJFILE, --injfile INJFILE     xml file for injection
  -F FOLLOWUP_INJECTIONS, --followup-injections FOLLOWUP_INJECTIONS         TODO; fill this out
  --bw-inject   BayesWave internal injections
  -G GRACEID, --graceID GRACEID     GraceID of event
  --graceID-list GRACEID_LIST       List of GRACEIDs
  --gdb-playground      Use GraceDB playground
  --submit-to-gracedb       Submit results to GraceDB *ONLY USE THIS IF YOU KNOW EXACTLY WHAT YOU'RE DOING*
  --skip-megapy     Skip megaplot plotting script
  --skip-post       Skip :code:`BayesWavePost`
  --separate-post-dag       Make a separate dag for postprocessing
  --trigger-time-delta TRIGGER_TIME_DELTA   Offset from trigger time
  --bayeswave-clean-frame      Make glitch subtracted .gwf frames
  --condor-precommand    Include conodr precommand for smart restart after checkpoint
  --condor-submit       Automatically submit dag to condor
  --bayeswave-retries BAYESWAVE_RETRIES     Number of times to retry on condor if job fails
  --osg-deploy      Deploy job on open science grid
  --transfer-files  Transfer files with condor job
  --shared-filesystem   Condor shared filesystem
  --singularity SINGULARITY     Use singularity


Launching the BayesWave Job
================================

Using Condor
---------------
If using condor, you will submit a `DAG input file <https://htcondor.readthedocs.io/en/latest/users-manual/dagman-workflows.html>`_.
The DAG will manage the workflow -- launching first the :code:`BayesWave` job, 
then :code:`BayesWavePost` job after the :code:`BayesWave` job completes,
and the :code:`megaplot` job after the postprocessing completes. 
If running large batches of jobs, the individual jobs will be launched in parallel.

When running :code:`bayeswave_pipe`, you can include :code:`--condor-submit` to launch the job automatically.
Otherwise, you can submit the dag from via

.. code-block:: console

    $ condor_submit_dag WORKDIR.dag

where :code:`WORKDIR` is the directory name used in the :code:`bayeswave_pipe` call.

Without Condor
---------------

If you are running without condor (for example, running on your own laptop),
you can use the bash script to run BayesWave, the postproccessing, and plotting.
The bash script can be found in the working directory, names :code:`WORKDIR.sh`, where WORKDIR is the name of your directory.

.. note::

    Warning! If you are running a batch of jobs, for example a bunch of injections,
    this will run all of those in sequence. So be mindful of how you use this script!
