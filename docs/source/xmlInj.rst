================================================
Simulated signal from xml file
================================================

:code:`BayesWave` can inject simulated signals from an XML file in the LIGO-specific :code:`LIGO_LW` format.

Making an XML file
---------------------

Here we show one example of how to make an XML file that contains parameters for simulated binary black hole signals.

.. todo::
    Reference a better source for making xml files

.. code:: bash

    #!/bin/bash


    ############################################################
    # Example script to make CBC xml injections.
    # This script produces injections for a 35-35Msun system.
    ############################################################

    seed=1234
    gpsstart=1254458039
    gpsend=1254458139
    fixmass=35
    outfile=equalMass35Msun-injections.xml

    lalapps_inspinj \
        --seed ${seed} --f-lower 20 --gps-start-time ${gpsstart} \
        --gps-end-time ${gpsend} \
        --amp-order 0 --waveform IMRPhenomDtwoPointFivePN \
        --time-step 10 --l-distr random \
        --i-distr uniform --disable-spin \
        --m-distr fixMasses --fixed-mass1 ${fixmass} \
        --fixed-mass2 ${fixmass} \
        --output ${outfile} \
        --snr-distr volume \
        --min-snr 10 --max-snr 35 \
        --ligo-fake-psd LALAdLIGO \
        --virgo-fake-psd LALAdVirgo \
        --ligo-start-freq 16 \
        --virgo-start-freq 16 \
        --ifos H1,L1 --verbose

Option 1: Injecting into simulated noise
-------------------------------------------

Config file
~~~~~~~~~~~~~~~

.. code:: ini

    [input]
    dataseed=1234
    seglen=4.0
    window=1.0
    flow=20
    srate=2048
    ifo-list=['H1','L1']

    [engine]
    ;Paths to bayeswave executables at run time.  These should generally be
    ; prefixed with e.g., /home/jclark/opt/bayewave for personal
    ; installations or  /opt/bayeswave/bin for containerized applications
    ; On CIT, you can use an igwn evironment (igwn-py39-testing recommended), and set this to /cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/envs/igwn-py39-testing/bin
    install=/home/meg.millhouse/Development_BW/stable/opt
    bayeswave=%(install)s/bin/BayesWave
    bayeswave_post=%(install)s/bin/BayesWavePost
    megaplot=%(install)s/bin/megaplot.py



    [datafind]
    ; Use simulated Gaussian noise
    frtype-list={'H1':'LALSimAdLIGO','L1':'LALSimAdLIGO'}
    channel-list={'H1':'H1:LALSimAdLIGO','L1':'L1:LALSimAdLIGO'}
    url-type=file


    [bayeswave_options]
    ; command line options for BayesWave.  See BayesWave --help
    updateGeocenterPSD=
    waveletPrior=
    Dmax=100
    noClean=
    ; Note: for real runs, set Niter to at least 1000000, or delete entirely to use BW defaults
    Niter=10000

    [bayeswave_post_options]
    ; command line options for BayesWavePost.  See BayesWavePost --help
    0noise=


    [injections]
    ; the events specifies which events from the xml file you want to run on.
    ; Can specify events like: events=all or events=5-10 or events=1,3,5 
    events=0-9 
    inj-fref=20

    [condor]
    universe=vanilla
    checkpoint=
    bayeswave-request-memory=1000
    bayeswave_post-request-memory=2000
    bayeswave-request-disk=100
    bayeswave_post-request-disk=100
    datafind=/usr/bin/gw_data_find
    ligolw_print=/usr/bin/ligolw_print
    segfind=/usr/bin/ligolw_segment_query_dqsegdb
    ; see e.g., https://ldas-gridmon.ligo.caltech.edu/ldg_accounting/user
    accounting-group = ligo.dev.o4.burst.paramest.bayeswave

Call to :code:`bayeswave_pipe`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash

    #!/bin/bash

    # Change to your BW installation
    BW_PREFIX=/Users/mmillhouse3/opt/lscsoft/bayeswave
    source ${BW_PREFIX}/etc/bayeswave-user-env.sh

    # Change this to point to your specific injection file
    injfile=equalMass35Msun-injections.xml

    bayeswave_pipe \
          --workdir xml_injections \
          -I ${injfile} \
          --sim-data \
           config.ini



Option 2: Injecting into real data
-------------------------------------------

.. todo::
    fill this in
