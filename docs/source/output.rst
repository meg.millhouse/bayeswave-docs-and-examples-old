========================
Using BayesWave Output
========================

This section will discuss the output files of BayesWave and how to use them.

BayesWave
-------------

Chain files
==============
The output of the BayesWave MCMC lives in the :code:`chains` directory.

For the :code:`signal` model, the file is labeled :code:`signal_params.dat.0`. The columns of the file are: Number of wavelets, right ascension, sin(declination), psi, ellipticity, phi_extrinsic, scale, 
:math:`t_{0_1}`, :math:`f_{0_1}`, :math:`Q_{1}`, :math:`A_{1}`, :math:`\phi_{0_1}`,..., :math:`t_{0_N}`, :math:`f_{0_N}`, :math:`Q_{N}`, :math:`A_{N}`, :math:`\phi_{0_N}`,
where the subscript :math:`N` is the number of wavelets.

For the :code:`glitch` model, there will be one chain file per detector, and will be labeld :code:`glitch_params_IFO.dat.0`, where :code:`IFO` will be H1, L1, V1, or K1. 
The columns of each file are: Number of wavelets, 
:math:`t_{0_1}`, :math:`f_{0_1}`, :math:`Q_{1}`, :math:`A_{1}`, :math:`\phi_{0_1}`,..., :math:`t_{0_N}`, :math:`f_{0_N}`, :math:`Q_{N}`, :math:`A_{N}`, :math:`\phi_{0_N}`,
where again the subscript :math:`N` is the number of wavelets.

 .. note::
    Keep in mind that the chain parameter files have irregular numbers of columns!
    Each row for signal chain will have :math:`7+5*N` entries, and each row for the glitch chain will have :math:`1+5*N`
    If using the :code:`chirplets` option, the chains will also contain the parameter :math:`\beta` for each wavelet.

The chains are thinned by a factor of 100, so each chain file should be :code:`Niter/100` rows long.

Evidence files
==============
The file :code:`evidence.dat` contains the evidence for each model. The first column is the model name, the second column is the evidence, and the third column is the error on the evidence estimation.

PSDs
==============
If running with the :code:`bayesLine` option enabled, BayesWave will also return PSDs (and ASDs) designed to be used with parameter estimation (such as Bilby).

.. todo::
    Someone who does PSD stuff should give more details on the PSD and how to use it with PE

BayesWavePost
---------------

This section highlights the most relevant files produced by :code:`BayesWavePost` for the typical user.

Waveform Reconstructions
============================

The waveform reconstructions take the chain files of raw wavelet parameters and extrinsic parameters and make full waveforms.
The waveform reconstructions produced by BayesWavePost include the median, 50% credible intervals, and 90% credible intervals.


* Time-domain reconstructions: 
    The whitened time-domain reconstructions can be found in the :code:`post` directory in :code:`MODEL/MODEL_median_time_domain_waveform_IFO.dat`, 
    where :code:`MODEL` is :code:`signal` or :code:`glitch`, and :code:`IFO` is :code:`H1`, :code:`L1`, etc.
    The columns are: Time (seconds), median whitened :math:`h(t)`, 50% credible interval lower bound on :math:`h(t)`, 50% credible upper bound, 90% credible lower bound, 90% credible upper bound.

* Frequency spectra:
    The reconstructed waveform in the frequency domain can be found in the :code:`post` directory in :code:`MODEL/MODEL_median_frequency_domain_waveform_spectrum_IFO.dat`,
    where :code:`MODEL` and :code:`IFO` are as above.  The columns are: Frequency (Hz), median :math:`h(f)`, 50% credible interval lower bound, 50% credible upper bound, 90% credible lower bound, 90% credible upper bound.


Waveform Moments and other Characteristics
========================================================

In addition to waveform reconstructions, :code:`BayesWavePost` calculates waveform moments as a way to characterize the GW without assuming prior models.
The four moments calcualted are: central time, duration, central frequency, and bandwidth. 
The posterior distribution of these moments are in the :code:`post` directory, in the files `MODEL/MODEL__whitened_moments_IFO.dat`, where again :code:`MODEL` and :code:`IFO` are as above.
This file also contains the posterior distribution of SNRs.

.. todo::
    The waveform moments file overall has a lot of superfluous stuff. Should I include all that here, or should we change in :code:`BayesWavePost`?

Q-Scans
===================

Q-scans are a common tool used in LIGO and Virgo to view data in a time-frequency representation.
:code:`BayesWavePost` produces data for Q-scans for the data, recovered waveform, and residual (data minus recovered waveform).

.. todo::
    Spectrogram naming scheme is a mess. Need a better way to label them so we can describe what they are.

Injections
===================

If the :code:`BayesWave` run was done on an injection, then :code:`BayesWavePost` also saves information about the injected signal.

* Injected waveforms:

    * Whitened time domain: :code:`post/injected_whitened_waveform_IFO.dat`
    * Frequency spectrum: :code:`post/injected_whitened_spectrum_IFO.dat`

* Injected waveform moments: :code:`post/injection_whitened_moments_IFO.dat`
* Spectrograms of injected signals: :code:`post/injected_spectrogram_Q_IFO.dat`, where :math:`Q=4,8,16`
* Overlaps the posterior distribution of overlaps between the injected and recovered signals can be found in the waveform moments file from above

Megaplot and Output Pages
--------------------------

The postprocessing from :code:`BayesWavePost` is read in by :code:`megaplot.py` to make plots, and output webpages to easily digest the results of your run.
An example output page can be found ...

.. todo::
    Make a good example output page and then don't touch it