========================
Algorith Overview
========================

This section gives a general overview of the :code:`bayeswave` algorithm.  For more details, please see Cornish and Littenberg 2015, Littenberg and Cornish 2015, and Cornish et al. 2021.

Basis functions
-------------------------

BayesWave reconstructs non-Gaussian features in gravitational-wave detector data using a sum of basis functions.
The standard basis function are sine-Gaussian (also called Morlet-Gabor) wavelets, defined in the time domain as

.. math::

    \Psi(t) = Ae^{-(t-t_0)^2/\tau^2}\cos\left(2\pi f_0(t-t_0)+\phi_0\right)

with :math:`\tau=Q/(2\pi f_0)`.  Each wavelet is uniquely defined by the parameters :math:`t_0,\ f_0,\ Q,\ A,\ \phi_0`.  Sine-Gaussians are an overcomplete basis, and technically a frame as opposed to a basis.

In addition to wavelets, BayesWave can also use Chirplets as the basis of reconstruction, where the chirplets are a sine-Gaussian wavelet with linear frequency evolution (see Millhouse et al. 2018 for details).
BayesWave can also use a CBC model in some cases (more details in the section below).

Reversible jump Markov chain Monte Carlo
--------------------------------------------

The five parameters of the wavelets (or six parameters for chirplets) are marginalized over using a reversible jump Markov chain Monte Carlo (RJMCMC).
By allowing the number of wavelets to be a parameter in our model we can reconstruct the underlying signal without over fitting and fitting for noise.

The result is a posterior distribution on :math:`h(f)` (or :math:`h(t)`).

BayesWave can also do model comparison to distinguish between astrophysical signals and instrumental glitches. Here we overview the different BayesWave models.

Signal model
=================

The signal model assumes that the reconstructed waveform is coherent across detectors.
This means that there is a common set of wavelets, plus a set of extrinsic parameters (also marginalized over via RJMCMC) whic are used to forward project the waveform onto each detector.

Glitch model
=================

The glitch model reconstructs detector data independently, not assuming any coherence between detectors.  In the glitch model an independent set of wavelets is used for each detector.

Joint model
=================

The joint model (also sometimes referred to as the "signal+glitch model" or the "full model") 
assumes that detector data can contain both a coherent and incoherent signal, such as an instrumental glitch coincident with a GW signal.

CBC Model
=================

The CBC model assumes that the coherent part of the data is a compact binary coalesence, and the incoherent part uses sine-Gaussian wavelets as the basis. See Hourihane et al. 2021 for more details.
This is mainly used for glitch cleaning.

.. todo::
    Put more info about CBC model here

Model Selection
=================

For each model, BayesWave calculates the evidence using thermodynamic integration.  This is used to calculate the log signal-to-glitch Bayes factor, :math:`\ln\mathcal{B}_{S,G}`, which gives the relative evidence between the signal containing only a signal vs only a glitch (assuming equal prior odds).

For the Joint model, the Bayes factors are calculated as the ratio of the number of iterations spent in each model.
The Bayes factors reported for the joint model are: 1. the Bayes factor for a signal only vs a signal plus a glitch, and 2. the Bayes factor for a glitch only vs a signal plus glitch.
(TODO: I think this last part is kinda confusing)

Power Spectral Density Estimation
--------------------------------------------

.. todo::
    Can someone else please write about BayesLine?