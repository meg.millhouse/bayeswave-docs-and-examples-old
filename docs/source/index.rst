.. BayesWave documentation master file, created by
   sphinx-quickstart on Wed Dec 14 14:46:37 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to BayesWave's documentation!
=====================================

A Bayesian algorithm designed to robustly distinguish gravitational wave signals
from noise and instrumental glitches without relying on any prior assumptions of waveform morphology.
BayesWave produces posterior probability distributions directly on gravitational wave
signals and instrumental glitches, allowing robust, morphology-independent waveform reconstruction.
See arXiv:1410.3835 for an introduction to the BayesWave
algorithm.
LSC/Virgo members: See the BayesWave wiki for further info

.. toctree::
   :maxdepth: 12
   :caption: General Info:


   gettingStarted
   fairUse
   workflow
   algo
   setupRun
   output

.. toctree::
   :maxdepth: 12
   :caption: Examples:

   xmlInj
   GW150914
   MDC

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
