#!/bin/bash

# If using system install on CIT, you can delete these tow lines 
BW_PREFIX=/home/meg.millhouse/Development_BW/stable
source ${BW_PREFIX}/etc/bayeswave-user-env.sh

# Change this to point to your specific injection file
injfile=equalMass35Msun-injections.xml

bayeswave_pipe \
          --workdir xml_injections \
          -I ${injfile} \
          --sim-data \
           config.ini

# The --sim-data there means you're running on simulated data
