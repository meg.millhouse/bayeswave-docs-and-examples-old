#!/bin/bash


############
# Example script to make CBC xml injections.
# This script produces injections for a 35-35Msun system.
############

seed=1234
gpsstart=1254458039
gpsend=1254458139
fixmass=35
outfile=equalMass35Msun-injections.xml

    lalapps_inspinj \
        --seed ${seed} --f-lower 20 --gps-start-time ${gpsstart} \
        --gps-end-time ${gpsend} \
        --amp-order 0 --waveform IMRPhenomDtwoPointFivePN \
        --time-step 10 --l-distr random \
        --i-distr uniform --disable-spin \
        --m-distr fixMasses --fixed-mass1 ${fixmass} \
        --fixed-mass2 ${fixmass} \
        --output ${outfile} \
        --snr-distr volume \
        --min-snr 10 --max-snr 35 \
        --ligo-fake-psd LALAdLIGO \
        --virgo-fake-psd LALAdVirgo \
        --ligo-start-freq 16 \
        --virgo-start-freq 16 \
        --ifos H1,L1 --verbose
