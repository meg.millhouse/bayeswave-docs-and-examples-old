# BayesWave Docs and Examples

This documentation is still a work in progress!

# Getting Started

## Installation

### Option 1: Running on CIT
If you are running on CIT, you can use environments with already existing `BayesWave` installations instead of installing yourself. The recommended environment is currently `igwn-py39-testing`.

### Option 2: Conda forge
`conda install -c conda-forge bayeswave`

### Option 3: Installing from source
*Note: it is recommended to follow the fork/merge development model*

Clone from the `bayeswave` repository

`git clone git@git.ligo.org:lscsoft/bayeswave/git`

Set up the conda environment. From the main `bayeswave` directory, run the command

`conda env create -f environment.yaml`

Activate the environment via

`conda activate bayeswave`

Still from the `bayeswave` directory, install via

```
export BAYESWAVE_PREFIX=${HOME}/opt/lscsoft/bayeswave
./install.sh ${BAYESWAVE_PREFIX}
```
*Note: instead of `${HOME}/opt/lscsoft/bayeswave` you can also use a directory of your choosing*

Upon successful installation, you should be prompted to source the environment via

`source ${BAYESWAVE_PREFIX}/etc/bayeswave-user-env.sh`

*TODO: is it confusing that there are two environments??*

## BayesWave Workflow

### `bayeswave_pipe`
This reads in a config file that contains information such as what data to look at, whether these are injections, and general run setting. This then sets up the BayesWave job(s) to be submitted to condor. This can produce a dag file for either a single event, or a batch of events. `bayeswave_pipe` uses the pythonm module `ConfigArgParse`.

### `BayesWave`
This is the main part of the code. This reads in detector data (real or simulated) and performs the RJMCMC. The main outputs of `BayesWave` are chain files.

### `BayesWavePost`
Processes the raw chains output by `BayesaWave` to make more human-readable outputs, such as posterior distributions on waveforms.  `BayesWavePost` is not always required -- for example for large batches of runs where only the Bayes factor is of interest it is not recommended to run `BayesWavePost` for every trigger to save both computing time and disk space. To skip this part, include `--skip-post` in the `bayeswave_pipe` call.

### `megaplot`
Can only be run after `BayesWavePost` has been run. Uses the output of `BayesWavePost` to make plots and output webpages. Again, `megaplot` is not required for every run. If you are not planning on looking at the output webpages, there is no need to run `megaplot`. To skip this part, include `--skip-megapy` in the `bayeswave_pipe` call.

### `megasky`
NOW DEFUNCT. 

## Making an config file

A config file will be a `.ini` file, and follows common .ini  syntax.  Sections are labeled in `[square brackets]`, 

`[input]` -- General settings for the run

Required:

- `seglen=float`: duration of data (s)

- `window=float`: duration of time window for model characterization runs (i.e the window where wavelets will can be placed)

- `flow=float`: minimum frequency (Hz)

- `srate=float`: sampling rate (Hz). Must be a power of 2.

- `PSDlength=float`: GPS start time for PSD estimation

- `ifolist=float`: List of IFOs to include in the analysis, for example `['H1','L1']`


Optional:

- `dataseed=integer`: Required if using simulated noise. Seed for noise realisation. Integer.


`[engine]` -- Information about where BayesWave and related scripts are

Required:

- `bayeswave=str`: Path to the `BayesWave` executabe, for example `{path_to_executables}/BayesWave`
- `bayeswave_post=str`: Path to `BayesWavePost` executable, e.g. `{path_to_executables}/BayesWavePost`
- `megaplot=str`: Path to megaplot executable, e.g. `{path_to_executables}/megaplot.py`


`[datafind]` -- information for `gw_data_find`

Required:

- `frtype-list=dict`: List of frame times for each IFO. example: `{'H1':'LALSimAdLIGO','L1':'LALSimAdLIGO'}`

- `channel-list=dict`: List of channels for each IFO. example: `{'H1':'H1:LALSimAdLIGO','L1':'L1:LALSimAdLIGO'}`

- `url-type=file` -- information about how cache is made. 

Optional:

`veto-categories=list`: Data quality vetoes



`[bayeswave_options]` -- command line options for `BayesWave`.  See BayesWave --help for a detailed list.
- Any flag can by passed to the `BayesWave` command line by setting `exampleFlag=`, and a flag with an argument can passed with `exampleFlag=arugment`.

`[bayeswave_post_options]` -- command line options for `BayesWavePost`.

Required:

`0noise=`: Always required for technical purposes.

`[injections]` -- Optional. Use if doing injections from an xml file.

If using, required:

- `events=list of events`: Flexible syntax. Can set to `all`, or for a smaller set of events a list like `0:10` or `1,3,5`.

- `inj-fref=float`: Reference frequency for injection.

*TODO: include MDC section*



`[condor]` -- information for setting up the condor job.

Required:
- `universe=vanilla`
- `bayeswave-request-memory=int`: Required memory for `BayesWave`
- `bayeswave_post-request-memory=int`: Required memory for `BayesWavePost`
- `bayeswave-request-disk=int`: Required disk space for `BayesWave`
- `bayeswave_post-request-disk=int`: Required disk space for `BayesWavePost`
- `datafind=/usr/bin/gw_data_find`: Specify the `gw_data_find` executable 
- `ligolw_print=/usr/bin/ligolw_print`: Specify the `ligolw_print` executable 
- `accounting-group=str` LIGO accounting group

Optional:
- `checkpoint=`: Self checkpointing. If running on condor this should be used.
