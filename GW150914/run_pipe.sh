#!/bin/bash


##################################
# Make a job for GW150914
# Note: this uses weird settings for a quick run. Results may not be scientifically meaningful.
##################################

BW_PREFIX=/home/meg.millhouse/Development_BW/opt/
source ${BW_PREFIX}/etc/bayeswave-user-env.sh

bayeswave_pipe GW150914.ini \
    --trigger-time 1126259462.420000076 \
    --workdir LDG-GW150914_gluetest
